#!/usr/bin/env python
from setuptools import setup, find_packages

from accounts import VERSION


setup(name='cartwear-accounts',
      version=VERSION,
      author="Ray Ch",
      author_email="ray@ninjaas.com",
      description="Managed accounts for django-cartwear",
      long_description=open('README.md').read(),
      license=open('LICENSE').read(),
      packages=find_packages(exclude=['sandbox*', 'tests*']),
      include_package_data=True,
      # See http://pypi.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Web Environment',
          'Framework :: Django',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: BSD License',
          'Operating System :: Unix',
          'Programming Language :: Python'],
      install_requires=['django-cartwear>=0.5',
                        'python-dateutil>=2.1,<2.2'])
